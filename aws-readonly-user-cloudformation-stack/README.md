# aws-readonly-user-cloudformation-stack

Creates read-only AWS IAM user for cloud analysis purposes of MaxSpotter.

Has access to read:

 - AWS Resources
 - Billing (without payment etc.)
 

[Lauch Stack on your AWS Console](https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/new?stackName=maxspotter-readonly-stack&templateURL=https%3A%2F%2Fmaxspotter.s3.amazonaws.com%2Fmaxspotter_readonly.yaml)

[Lauch Stack on your AWS Console Ireland](https://console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks/new?stackName=maxspotter-readonly-stack&templateURL=https%3A%2F%2Fmaxspotter.s3.amazonaws.com%2Fmaxspotter_readonly.yaml)

To enable billing access:
 - Go to "My Account" from AWS Console top menu
 - Find ¨IAM User and Role Access to Billing Information¨ and select "Edit"
 - Enable ¨Activate IAM Access¨ and click "Update"




