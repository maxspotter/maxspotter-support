# aws-readonly-user-cloudformation-stack

Creates AWS IAM role for cloud analysis purposes of MaxSpotter.

Has access to read/write:

 - Billing (without payment etc.)
 

[Lauch Stack on your AWS Console](https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/new?stackName=maxspotter-write-role-stack&templateURL=https%3A%2F%2Fmaxspotter.s3.amazonaws.com%2Fmaxspotter-write-role.yaml)




