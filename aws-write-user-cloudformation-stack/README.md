# aws-readonly-user-cloudformation-stack

Creates read-only AWS IAM user for cloud analysis purposes of MaxSpotter.

Has access to write:

 - EC2
 - VPC
 - Security Groups
 - RDS
 - CloudFormation
 - IAM
 

[Lauch Stack on your AWS Console](https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/new?stackName=maxspotter-write-stack&templateURL=https%3A%2F%2Fmaxspotter.s3.amazonaws.com%2Fmaxspotter_write.yaml)

[Lauch Stack on your AWS Console Ireland](https://console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks/new?stackName=maxspotter-write-stack&templateURL=https%3A%2F%2Fmaxspotter.s3.amazonaws.com%2Fmaxspotter_write.yaml)



